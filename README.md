README
------
This repository contains the book editors' version of the PMWE volume entitled " Multiword expressions at length and in depth. Extended papers from the MWE 2017 workshop", edited by:
  * [Stella Markantonatou](http://www.ilsp.gr/en/profile/staff?view=member&id=38&task=show)
  * [Carlos Ramisch](http://pageperso.lis-lab.fr/carlos.ramisch/)
  * [Agata Savary](http://www.info.univ-tours.fr/~savary/)
  * [Veronika Vincze](http://www.inf.u-szeged.hu/~vinczev/index_en.html)
  
We (the volume editors) are now in the process of applying the proofreaders' comments. If some remarks require the authors' input, we will contact them accordingly. 

See the [pdf file](proofreading/proofreading-version.pdf) for the version submitted to community proofreading at Language Science Press (LSP) in July 2018. 
See also the same [indexed version](proofreading/indexed-version.pdf) highlighting the indexed terms and languages.

For the proofreaders' comments see:
  * The [PaperHive](https://paperhive.org/documents/items/aDomKdUBfbkP) version
  * Additional comments from [proofreader 1](proofreading/proofreading-comments-1.pdf)
  * Additional comments from [proofreader 2](proofreading/proofreading-comments-2.pdf)
  
You might also be interested in understanding how the [workflow for edited volumes](https://userblogs.fu-berlin.de/langsci-press/2017/08/25/workflow-for-edited-volumes/) works at LSP.
